Provides tokens that render full entities using view mode specified.

Generally the token provided is [entity_type:render:view_mode_name] (e.g.
[node:render:teaser]).
